/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.49407180862206207, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5456650167651286, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [0.47399296050058665, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.0018656716417910447, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [0.5234401930368838, 500, 1500, "suggestProgram"], "isController": false}, {"data": [0.5427695287282117, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.5246179966044142, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.4572460859092734, 500, 1500, "findAllClass"], "isController": false}, {"data": [0.537467700258398, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [0.5298507462686567, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.5389357560025957, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [0.5432553668695931, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.4547325102880658, 500, 1500, "registrationByID"], "isController": false}, {"data": [0.09094256259204712, 500, 1500, "leadByID"], "isController": false}, {"data": [0.5018382352941176, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.5166200139958013, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [0.0017006802721088435, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.5187028140013726, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [0.39726651480637815, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 47738, 0, 0.0, 1144.7799028028007, 37, 26744, 973.0, 1598.0, 1945.0, 10550.970000000005, 155.5556279672974, 960.3418830159799, 292.473135470059], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 6263, 0, 0.0, 869.2481239022846, 48, 2688, 846.0, 1334.0, 1479.7999999999993, 1807.5199999999977, 20.846925053590212, 12.276070124330955, 25.727403643718297], "isController": false}, {"data": ["getCentreHolidaysOfYear", 2557, 0, 0.0, 1068.0336331638648, 114, 2969, 1058.0, 1595.2000000000003, 1795.3999999999996, 2130.460000000001, 8.500608373614536, 21.72926070346939, 10.916308604788199], "isController": false}, {"data": ["getPastChildrenData", 268, 0, 0.0, 10282.936567164174, 1483, 13078, 11096.0, 12131.0, 12422.85, 12860.6, 0.8798106437390639, 1.7557389024444292, 2.0062088409479633], "isController": false}, {"data": ["suggestProgram", 2901, 0, 0.0, 940.3092037228532, 48, 2413, 932.0, 1399.4000000000005, 1554.9, 1886.0, 9.648804792139932, 7.820808571754047, 11.976202041806498], "isController": false}, {"data": ["findAllWithdrawalDraft", 3098, 0, 0.0, 879.5755326016786, 37, 2589, 851.0, 1350.0, 1503.0, 1872.1499999999965, 10.311851972665936, 6.676521345583512, 33.66457143029515], "isController": false}, {"data": ["findAllLevels", 2945, 0, 0.0, 925.0285229202041, 75, 2417, 902.0, 1404.8000000000002, 1590.6999999999998, 1948.54, 9.803758384793356, 6.242236784067645, 8.214477240383495], "isController": false}, {"data": ["findAllClass", 2491, 0, 0.0, 1091.91649939783, 165, 2503, 1064.0, 1567.8000000000002, 1716.0, 2092.9199999999983, 8.283397955586887, 825.0474684415757, 6.697903815650335], "isController": false}, {"data": ["getStaffCheckInOutRecords", 3096, 0, 0.0, 879.533591731265, 65, 2326, 858.0, 1345.3000000000002, 1496.1499999999996, 1872.119999999999, 10.305229171520821, 6.1086661202276735, 13.485358486169822], "isController": false}, {"data": ["getChildrenToAssignToClass", 3015, 0, 0.0, 903.6288557213916, 77, 2559, 879.0, 1380.4, 1546.1999999999998, 1888.3600000000006, 10.03574923608476, 5.713712699841226, 7.360202808886382], "isController": false}, {"data": ["getCountStaffCheckInOut", 3082, 0, 0.0, 884.2725502920174, 63, 2580, 857.0, 1361.7000000000003, 1539.85, 1851.2300000000014, 10.258561005485435, 6.060966219061219, 10.45892352512382], "isController": false}, {"data": ["searchBroadcastingScope", 132, 0, 0.0, 21121.719696969703, 4766, 26744, 23466.0, 25116.8, 25773.35, 26600.449999999993, 0.4301299513822812, 0.8331349345355248, 0.8430379027580454], "isController": false}, {"data": ["getTransferDrafts", 3121, 0, 0.0, 873.4867029798131, 67, 2685, 852.0, 1326.6000000000004, 1503.0, 1824.5599999999995, 10.388477810064941, 6.675408592795637, 18.027661199692773], "isController": false}, {"data": ["registrationByID", 2430, 0, 0.0, 1125.1600823045276, 118, 2984, 1119.0, 1712.9, 1930.0, 2391.07, 8.077356477341851, 13.53398680698442, 36.74250631978354], "isController": false}, {"data": ["leadByID", 1358, 0, 0.0, 2013.7820324005897, 249, 3777, 2081.5, 2694.3, 2895.1, 3289.6300000000037, 4.502921261879025, 12.198035717690379, 20.33350382317247], "isController": false}, {"data": ["getRegEnrolmentForm", 2720, 0, 0.0, 1003.3367647058847, 105, 2707, 1000.5, 1475.0, 1638.749999999999, 2011.58, 9.045560359161955, 16.180419515921184, 38.67330395743266], "isController": false}, {"data": ["getEnrollmentPlansByYear", 2858, 0, 0.0, 954.1389083275013, 92, 2528, 931.5, 1431.1, 1609.0999999999995, 1985.0, 9.502214301863205, 6.690523937151729, 15.765880955923425], "isController": false}, {"data": ["findAllLeads", 294, 0, 0.0, 9357.38435374149, 1455, 13018, 10085.5, 11067.5, 11214.0, 11660.250000000004, 0.9667745245047746, 1.9355304075661617, 2.188460300587957], "isController": false}, {"data": ["getAvailableVacancy", 2914, 0, 0.0, 935.8980782429658, 90, 2538, 916.0, 1422.5, 1607.0, 1986.5499999999997, 9.691559629366024, 5.309536086010097, 12.824280564248989], "isController": false}, {"data": ["getRegistrations", 2195, 0, 0.0, 1244.4264236902027, 138, 3125, 1240.0, 1886.4, 2116.0, 2457.6399999999994, 7.296261455462889, 17.178964625804834, 22.53005734587271], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 47738, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
